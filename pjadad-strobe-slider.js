(function() {
	var PSS = function(el, options) {
		if (el === void 0) {
			throw new Error('Element is missing');
		}

		this.$el = $(el);
		
		// extend options
		this.options = $.extend({}, this.defaults, options);

		// build item-data
		this.itemData = this.$el
			.find('.pss__item')
			.map(function() {
				return this.innerHTML;
			})
			.toArray(); // convert from jquery collection to regular array

		// create set element with it's columns
		var setHTML = [];
		var halfGutter = this.options.gutter / 2;
		var columnWidth = 100 / this.options.columnCount;
		
		setHTML.push('<div class="pss__set" style="margin-left: -' + halfGutter + 'px; margin-right: -' + halfGutter + 'px">');
		for (var i = this.options.columnCount-1; i >= 0; i--) {
			setHTML.push('<div class="pss__set-column" style="padding: 0 ' + halfGutter + 'px; width: ' + columnWidth + '%" />');
		}
		setHTML.push('</div>');
		this.$el.append(setHTML.join("\n"));

		// keep reference to the columns
		this.$setColumns = this.$el.find('.pss__set-column');

		// state
		this.isLoading = false;
		this.currentSetIndex = this.prevSetIndex = 0;

		// bind controls
		this.$el.find('.pss__next').on('click', this.loadNext.bind(this));
		this.$el.find('.pss__prev').on('click', this.loadPrev.bind(this));

		// initialize the first set of slides
		this.revealSet();
	};

	PSS.prototype = {
		defaults: {
			columnCount: 6,
			gutter: 30,
			strobeSpeed: 50
		},
		loadPrev: function() {
			if (this.currentSetIndex !== 0 && !this.isLoading) {
				this.isLoading = true;
				this.prevSetIndex = this.currentSetIndex;
				this.currentSetIndex--;
				this.revealSet();
			}
		},
		loadNext: function() {
			if ((this.currentSetIndex - 1) * this.options.columnCount < this.itemData.length / this.options.columnCount
					&& !this.isLoading) {
				this.isLoading = true;
				this.prevSetIndex = this.currentSetIndex;
				this.currentSetIndex++;
				this.revealSet();
			}
		},
		revealSet: function() {
			// slice items belonging to this set
			this.currentSetItems = this.itemData.slice(this.currentSetIndex * this.options.columnCount, 
				this.currentSetIndex * this.options.columnCount + this.options.columnCount);

			this.revealIntervalIndex = 0;

			// reveal items 
			this.revealInterval = setInterval(this.revealItem.bind(this), this.options.strobeSpeed);
		},
		revealItem: function() {
			var index;

			// match the index 'direction' with the direction of the 'slide'
			if (this.currentSetIndex > this.prevSetIndex) {
				index = this.options.columnCount - 1 - this.revealIntervalIndex;
			} else {
				index = this.revealIntervalIndex;
			}

			var data = this.currentSetItems[index] || '';

			// populate the corresponding column
			this.$setColumns.eq(index).html(data);

			// end interval and loading on last index
			if (this.revealIntervalIndex == this.options.columnCount - 1) {
				clearInterval(this.revealInterval);
				this.isLoading = false;
			}

			this.revealIntervalIndex++;
		}
	};

	// init
	$('.pss').each(function() {
		var $el = $(this);
		var options = {};

		if ($el.data('column-count')) {
			options.columnCount = $el.data('column-count');
		}

		if ($el.data('gutter')) {
			options.gutter = $el.data('gutter');
		}

		if ($el.data('strobe-speed')) {
			options.strobeSpeed = $el.data('strobe-speed');
		}

		new PSS(this, options);
	});

})();