# PJADAD Strobe Slider

### Dependencies
- jQuery

### Issues
- Sometimes you can press next without there being a set of items to display, causing the the slider to be empty

### Roadmap
- Add state classes to the controls, to highlight them as disabled/inactive
- Add responsive feature; different column count on different widths, eg.: { 768: 4, 1024: 6, 1280: 8 }
- Add tags and filter; posiblity to filter the list of items based on a tag, should refresh the slider with the interval effect